import java.util.List;

public interface BankApplicationMain {
	
	public abstract void addAccount(BankAccCreation userInfo);
	public abstract List<BankAccCreation> listAllAccounts();
	public abstract void deleteAccount(BankAccCreation userInfo);
	public abstract void fetchAccDetailsById(BankAccCreation userInfo);
	
}
