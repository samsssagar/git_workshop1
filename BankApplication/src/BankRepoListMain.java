import java.util.List;

public class BankRepoListMain {

	public static void main(String[] args) {
		BankRepositoryList b = new BankRepositoryList();
		
		BankAccCreation user1 = new BankAccCreation("SAMEER");
		BankAccCreation user2 = new BankAccCreation("SWETANSHU");
		BankAccCreation user3 = new BankAccCreation("HRITHIK");
		BankAccCreation user4 = new BankAccCreation("VIJAY");
		BankAccCreation user5 = new BankAccCreation("SHEERSH");
		BankAccCreation user6 = new BankAccCreation("RISHABH");
		BankAccCreation user7 = new BankAccCreation("MONTY");
		BankAccCreation user8 = new BankAccCreation("ANKIT");
		BankAccCreation user9 = new BankAccCreation("SAM");
		BankAccCreation user10 = new BankAccCreation("RITURAJ");
		
		b.addAccount(user1);
		b.addAccount(user2);
		b.addAccount(user3);
		b.addAccount(user4);
		b.addAccount(user5);
		b.addAccount(user6);
		b.addAccount(user7);
		b.addAccount(user8);
		b.addAccount(user9);
		b.addAccount(user10);
		
		
		List<BankAccCreation> listAllAccounts = b.listAllAccounts();
		for(int i = 0; i<listAllAccounts.size(); i++) {
			System.out.println(listAllAccounts.get(i).getName() +" "+ listAllAccounts.get(i).getAccountId());
		}
		
		b.deleteAccount(user1);
		b.deleteAccount(user2);
		
		System.out.println();
		List<BankAccCreation> list = b.listAllAccounts();
		for(int i = 0; i<list.size(); i++) {
			System.out.println(list.get(i).getName() +" "+ list.get(i).getAccountId());
		}
		
		System.out.println();
		b.fetchAccDetailsById(user3);
		
		
	}

}
