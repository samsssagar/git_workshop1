public class BankClient {

	public static void main(String[] args) {
		BankAccCreation user1 = new BankAccCreation("Sameer");
		BankAccCreation user2 = new BankAccCreation("Swetanshu");
		BankAccCreation user3 = new BankAccCreation("Sheersh");
		BankAccCreation user4 = new BankAccCreation("Hrithik");
		BankAccCreation user5 = new BankAccCreation("Ankit");
		BankAccCreation user6 = new BankAccCreation("Sarthak");
		BankAccCreation user7 = new BankAccCreation("Mukund");
		BankAccCreation user8 = new BankAccCreation("Vijay");
		BankAccCreation user9 = new BankAccCreation("Ankit");
		BankAccCreation user10 = new BankAccCreation("Rituraj");

		System.out.println("The List of Account Holders: ");
// System.out.println("Account Holder: "+user1.getName()+"\n"+"Account Id: "+user1.getAccountId()+"\n");
// System.out.println("Account Holder: "+user2.getName()+"\n"+"Account Id: "+user2.getAccountId()+"\n");
// System.out.println("Account Holder: "+user3.getName()+"\n"+"Account Id: "+user3.getAccountId()+"\n");

		BankRepository repoDetails = new BankRepository();
		repoDetails.addAccount(user1); // 0
		repoDetails.addAccount(user2); // 1
		repoDetails.addAccount(user3); // 2
		repoDetails.addAccount(user4); // 3
		repoDetails.addAccount(user5); // 4
		repoDetails.addAccount(user6); // 5
		repoDetails.addAccount(user7); // 6
		repoDetails.addAccount(user8); // 7
		repoDetails.addAccount(user9); // 8
		repoDetails.addAccount(user10);
		
		

		BankAccCreation[] bc = repoDetails.listAllAccounts();
		for (int i = 0; i < bc.length; i++) {
			System.out.println(bc[i].getAccountId() + " " + bc[i].getName());
		}

		System.out.println();
		System.out.println("Account Details after deleting the User Information:");
//repoDetails.deleteAccount(user8);
		BankAccCreation[] bc1 = repoDetails.deleteAccount(user8);
		for (int i = 0; i < bc1.length; i++) {
			System.out.println(bc1[i].getAccountId() + " " + bc1[i].getName());
		}
		repoDetails.fetchAccDetailsById(user1);
	}

}