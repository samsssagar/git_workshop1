import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class BankRepositoryList implements BankApplicationMain {

	List<BankAccCreation> list = new ArrayList<BankAccCreation>();
	// public static int index = 0;

	@Override
	public void addAccount(BankAccCreation userInfo) {

		list.add(userInfo);

	}

	@Override
	public List<BankAccCreation> listAllAccounts() {
		List<BankAccCreation> l = new ArrayList<BankAccCreation>();
		l.addAll(list);
		return l;
	}

	@Override
	public void deleteAccount(BankAccCreation userInfo) {
		Iterator<BankAccCreation> itr = list.iterator();
		while (itr.hasNext()) {
			BankAccCreation accountId = itr.next();
			if (accountId.getAccountId() == userInfo.getAccountId()) {
				list.remove(accountId);
				break;
			}
		}
	}

	@Override
	public void fetchAccDetailsById(BankAccCreation userInfo) {

		Iterator<BankAccCreation> itr = list.iterator();
		while(itr.hasNext()) {
			BankAccCreation accountId = itr.next();
			if(accountId.getAccountId() == userInfo.getAccountId()) {
				
				System.out.print(userInfo.getName()+" "+userInfo.getAccountId());
				
			}
		}
	}

	

}
